#include "CreativeSensor.h"

CreativeSensor::CreativeSensor(IImageTarget *perceptualDemo)
{
	this->mPerceptualDemo = perceptualDemo;
}

CreativeSensor::~CreativeSensor(void)
{
}

bool CreativeSensor::connect()
{
	//
	// -- Set up an SDK session
	//
	if(PXCSession_Create(&mSession) < PXC_STATUS_NO_ERROR)
	{
		OutputDebugString("Failed to create a session");
		return false;
	}

	//
	// -- Configure the video streams
	//
	PXCCapture::VideoStream::DataDesc request;
	memset(&request, 0, sizeof(request));
	request.streams[0].format = PXCImage::COLOR_FORMAT_RGB24;
	request.streams[0].sizeMin.width = request.streams[0].sizeMax.width = 1280;
	request.streams[0].sizeMin.height = request.streams[0].sizeMax.height = 720;
	request.streams[1].format = PXCImage::COLOR_FORMAT_DEPTH;

	//
	// -- Create the streams
	//
	mCapture = new UtilCapture(mSession);
	mCapture->LocateStreams(&request);

	//
	// -- Get the profiles the verify if we got the desired streams
	//
	PXCCapture::VideoStream::ProfileInfo colorProfile;
	mCapture->QueryVideoStream(0)->QueryProfile(&colorProfile);
	PXCCapture::VideoStream::ProfileInfo depthProfile;
	mCapture->QueryVideoStream(1)->QueryProfile(&depthProfile);

	//
	// -- Output to console
	//
	char line[64];
	sprintf(line, "Depth %d x %d\n", depthProfile.imageInfo.width, depthProfile.imageInfo.height);
	OutputDebugString(line);
	sprintf(line, "Color %d x %d\n", colorProfile.imageInfo.width, colorProfile.imageInfo.height);
	OutputDebugString(line);

	return true;
}

bool CreativeSensor::updateFrame()
{
	PXCSmartArray<PXCImage> images;
	PXCSmartSPArray syncPoints(1);

	pxcStatus status = mCapture->ReadStreamAsync(images, &syncPoints[0]);
	if(status < PXC_STATUS_NO_ERROR) return false;

	status = syncPoints.SynchronizeEx();
	if(syncPoints[0]->Synchronize(0) < PXC_STATUS_NO_ERROR) return false;

	//
	// -- get the color image
	//
	PXCImage *colorImage = mCapture->QueryImage(images, PXCImage::IMAGE_TYPE_COLOR);
	PXCImage::ImageData colorImageData;
	if(colorImage->AcquireAccess(PXCImage::ACCESS_READ, &colorImageData) < PXC_STATUS_NO_ERROR)
		return false;
	mPerceptualDemo->setColorBitmapData((char*)colorImageData.planes[0]);
	colorImage->ReleaseAccess(&colorImageData);

	//
	// -- get the depth image
	//
	PXCImage *depthImage = mCapture->QueryImage(images, PXCImage::IMAGE_TYPE_DEPTH);
	PXCImage::ImageData depthImageData;
	if(depthImage->AcquireAccess(PXCImage::ACCESS_READ, &depthImageData) < PXC_STATUS_NO_ERROR)
		return false;
	mPerceptualDemo->setDepthBitmapData((short*)depthImageData.planes[0]);
	depthImage->ReleaseAccess(&depthImageData);

	return true;
}


