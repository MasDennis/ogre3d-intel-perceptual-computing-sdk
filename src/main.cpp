#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#include "PerceptualDemo.h"

#ifdef __cplusplus
extern "C" {
#endif

INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
{
	PerceptualDemo *app = new PerceptualDemo();

	try
	{
		app->go();
	} catch(Ogre::Exception& e) {
		MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occured!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
	}

	return 0;
}

#ifdef __cplusplus
}
#endif
