#include "PerceptualDemo.h"


PerceptualDemo::PerceptualDemo(void)
{
}


PerceptualDemo::~PerceptualDemo(void)
{
}

void PerceptualDemo::createScene(void)
{
	//
	// -- Set up the camera
	//
	mCamera->setPosition(0, 0, 0);
	mCamera->setNearClipDistance(0.01);

	//
	// -- Create color rectangle and texture
	//
	mColorTexture = TextureManager::getSingleton().createManual("ColorTex", ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, TEX_TYPE_2D, 1280, 720, 0, PF_R8G8B8, TU_DYNAMIC);

	MaterialPtr material = MaterialManager::getSingleton().create("ColorMat", ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
	material->getTechnique(0)->getPass(0)->createTextureUnitState("ColorTex");

	Rectangle2D* rect = new Rectangle2D(true);
	rect->setCorners(-1.0, 1.0, 1.0, -1.0);
	rect->setMaterial("ColorMat");
	rect->setRenderQueueGroup(RENDER_QUEUE_BACKGROUND);

	AxisAlignedBox aabInf;
	aabInf.setInfinite();
	rect->setBoundingBox(aabInf);

	SceneNode* node = mSceneMgr->getRootSceneNode()->createChildSceneNode("ColorRect");
	node->attachObject(rect);

	//
	// -- Create depth rectangle and texture
	//
	mDepthTexture = TextureManager::getSingleton().createManual("DepthTex", ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, TEX_TYPE_2D, 320, 240, 0, PF_R8G8B8, TU_DYNAMIC);

	material = MaterialManager::getSingleton().create("DepthMat", ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
	material->getTechnique(0)->getPass(0)->createTextureUnitState("DepthTex");

	rect = new Rectangle2D(true);
	rect->setCorners(0, 0, 1.0, -1.0);
	rect->setMaterial("DepthMat");
	rect->setRenderQueueGroup(RENDER_QUEUE_BACKGROUND);
	rect->setBoundingBox(aabInf);

	node = mSceneMgr->getRootSceneNode()->createChildSceneNode("DepthRect");
	node->attachObject(rect);
	node->setVisible(true);

	//
	// -- Create a new sensor instance
	//
	mSensor = new CreativeSensor(this);
	mSensor->connect();
}

bool PerceptualDemo::frameStarted(const FrameEvent& evt)
{
	if(!mSensor->updateFrame()) return false;
}

void PerceptualDemo::setColorBitmapData(char* data)
{
	HardwarePixelBufferSharedPtr pixelBuffer = mColorTexture->getBuffer();
	pixelBuffer->lock(HardwareBuffer::HBL_DISCARD);
	const PixelBox& pixelBox = pixelBuffer->getCurrentLock();
	uint8* pDest = static_cast<uint8*>(pixelBox.data);

	for(size_t i=0; i<720; i++)
	{
		for(size_t j=0; j<1280; j++)
		{
			*pDest++ = *data++;
			*pDest++ = *data++;
			*pDest++ = *data++;
			*pDest++ = 255;
		}
	}

	pixelBuffer->unlock();
}

void PerceptualDemo::setDepthBitmapData(short* data)
{
	HardwarePixelBufferSharedPtr pixelBuffer = mDepthTexture->getBuffer();
	pixelBuffer->lock(HardwareBuffer::HBL_DISCARD);
	const PixelBox& pixelBox = pixelBuffer->getCurrentLock();
	uint8* pDest = static_cast<uint8*>(pixelBox.data);

	for(size_t i=0; i<240; i++)
	{
		for(size_t j=0; j<320; j++)
		{
			short depthVal = *data++ / 16;
			*pDest++ = depthVal;
			*pDest++ = depthVal;
			*pDest++ = depthVal;
			*pDest++ = 255;
		}
	}

	pixelBuffer->unlock();
}
