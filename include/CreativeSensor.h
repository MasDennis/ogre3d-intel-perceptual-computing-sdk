#pragma once

#include "pxcsession.h"
#include "pxcsmartptr.h"
#include "pxccapture.h"
#include "pxcface.h"
#include "util_capture.h"
#include <Windows.h>
#include "IImageTarget.h"

class CreativeSensor
{
public:
	CreativeSensor(IImageTarget *perceptualDemo);
	~CreativeSensor(void);

	bool connect();
	bool updateFrame();

private:
	PXCSmartPtr<PXCSession> mSession;
	UtilCapture *mCapture;
	IImageTarget *mPerceptualDemo;
};

