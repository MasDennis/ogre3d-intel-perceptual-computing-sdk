#pragma once
#include "BaseApplication.h"
#include "CreativeSensor.h"
#include "IImageTarget.h"

using namespace Ogre;

class PerceptualDemo :
	public BaseApplication, public IImageTarget
{
public:
	PerceptualDemo(void);
	virtual ~PerceptualDemo(void);
	bool frameStarted(const FrameEvent& evt);
	void setColorBitmapData(char* data);
	void setDepthBitmapData(short* data);

protected:
	CreativeSensor *mSensor;
	TexturePtr mColorTexture;
	TexturePtr mDepthTexture;

	virtual void createScene(void);
};


