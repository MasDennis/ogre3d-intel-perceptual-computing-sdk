#pragma once

class IImageTarget
{
public:
	virtual void setColorBitmapData(char* data) = 0;
	virtual void setDepthBitmapData(short* data) = 0;
};
